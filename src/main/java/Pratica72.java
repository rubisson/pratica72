
import java.io.IOException;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica72 {
    public static void main(String[] args) throws IOException {
        ContadorPalavras pal = new ContadorPalavras("C:\\Users\\Rubinho\\Documents\\NetBeansProjects\\Pratica72\\arqTest.txt");
        System.out.println("contaLetra >>> " + pal.getPalavras());
    
    }
}
