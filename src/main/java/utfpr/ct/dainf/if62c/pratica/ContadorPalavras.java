/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Rubinho
 */
public class ContadorPalavras {
    private BufferedReader reader;
    private File arquivo;
    private int contaLetra = 0;
    public ContadorPalavras(String file) throws IOException{
        arquivo = new File(file);
    }
    
    public int getPalavras(){
        try {
            if (!arquivo.exists()) {
                //cria um arquivo (vazio)
                arquivo.createNewFile();
            }
            //A classe FileReader recebe como argumento o objeto File do arquivo a ser lido:
            //construtor que recebe o objeto do tipo arquivo
            FileReader fr = new FileReader(arquivo);
            //construtor que recebe o objeto do tipo FileReader
            reader = new BufferedReader(fr);
            //equanto houver mais linhas
            while( reader.ready() ){
                //lê a proxima linha
                String linha = reader.readLine();
                System.out.println(linha);
                contaLetra = contaLetra + linha.length();
            }
            System.out.println(contaLetra);
            reader.close();
            fr.close();
        }catch (IOException ex){
        }
        return contaLetra;
    }






}